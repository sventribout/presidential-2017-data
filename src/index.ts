import * as restify from 'restify';
import { initRedisConnection } from './cache/redis';
import { initRouter } from "./router/router";

var server = restify.createServer();
server.use(restify.plugins.queryParser());

initRedisConnection()
initRouter(server)

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});
