import * as restify from "restify";
import * as axios from "axios";
import { getKey, setKey } from "../cache/redis";

const datasetIds = [
  "elections-presidentielles-2017-1ertour",
  "elections-presidentielles-2017-2emetour"
];

/*interface Dataset {
  id: string
}

async function getDatasets(offset: number = 0, limit: number = 100, datasets: Dataset[] = []): Promise<Dataset[]> {
  const url = `https://opendata.paris.fr/api/v2/catalog/datasets?offset=${offset}&limit=${limit}`
  try {
    const result = await axios.default.get(url)

    const total = result.data.total_count

    for (const datasetElt of result.data.datasets) {
      datasets.push({
        id: datasetElt.dataset.dataset_id
      })
    }

    if (offset < total) {
      offset += limit
      return getDatasets(offset, limit, datasets)
    }
    return datasets
  } catch (err) {
    console.log("err", err)
  }
  return datasets
}

function isDatasetPresent(id: string, datasets: Dataset[]): boolean {
  for (const dataset of datasets) {
    if (dataset.id === id) {
      return true
    }
  }
  return false
}*/

interface Coordinates {
  lat: number;
  lng: number;
}

interface Record {
  id: string;
  bvote_id: string;
  num_arrond: number;
  nb_inscr: number;
  nb_votant: number;
  dupont_aignan_nicolas: number;
  le_pen_marine: number;
  macron_emmanuel: number;
  hamon_benoit: number;
  arthaud_nathalie: number;
  poutou_philippe: number;
  cheminade_jacques: number;
  lassalle_jean: number;
  melenchon_jean_luc: number;
  asselineau_francois: number;
  fillon_francois: number;
  coordinates: Coordinates[]
}

interface PaginateRecords {
  pagination?: {
    offset: number;
    limit: number;
    total: number;
  },
  records: Record[]
}

async function getRecords(datasetId: string, offset: number, limit: number, round: number): Promise<PaginateRecords> {
  const url = `https://opendata.paris.fr/api/v2/catalog/datasets/${datasetId}/records?offset=${offset}&limit=${limit}`

  const records: Record[] = []
  try {
    const result = await axios.default.get(url)
    const total = result.data.total_count
    for (const recordElt of result.data.records) {

      const coordinates: Coordinates[] = []
      for (const coordinateArr of recordElt.record.fields.geo_shape.geometry.coordinates) {
        for (const cordinate of coordinateArr) {
          coordinates.push({
            lng: cordinate[0],
            lat: cordinate[1]
          })
        }
      }
      records.push({
        id: recordElt.record.id,
        bvote_id: recordElt.record.fields.id_bvote,
        num_arrond: recordElt.record.fields.num_arrond,
        nb_inscr: recordElt.record.fields.nb_inscr,
        nb_votant: recordElt.record.fields.nb_votant,
        // 1er tour
        dupont_aignan_nicolas: recordElt.record.fields.dupont_aignan_nicolas,
        le_pen_marine: recordElt.record.fields.le_pen_marine || recordElt.record.fields.marine_le_pen,
        macron_emmanuel: recordElt.record.fields.macron_emmanuel || recordElt.record.fields.emmanuel_macron,
        hamon_benoit: recordElt.record.fields.hamon_benoit,
        arthaud_nathalie: recordElt.record.fields.arthaud_nathalie,
        poutou_philippe: recordElt.record.fields.poutou_philippe,
        cheminade_jacques: recordElt.record.fields.cheminade_jacques,
        lassalle_jean: recordElt.record.fields.lassalle_jean,
        melenchon_jean_luc: recordElt.record.fields.melenchon_jean_luc,
        asselineau_francois: recordElt.record.fields.asselineau_francois,
        fillon_francois: recordElt.record.fields.fillon_francois,
        coordinates: coordinates
      })
    }

    return {
      pagination: {
        offset: offset,
        limit: limit,
        total: total
      },
      records: records
    }

  } catch (err) {
    console.log("err", err)
  }

  return {
    records: records
  }
}

async function getElectionOffices(req: restify.Request, res, next) {
  try {
    res.contentType = 'json'
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    // Check already in the cache
    const result = await getKey(req.url)
    if (result !== null && result !== undefined) {
      res.send(JSON.parse(result));
    } else {
      const records = await getRecords(datasetIds[parseInt(req.query.round || 1) - 1], req.query.offset, req.query.limit, req.params.round);
      res.send(records);
      await setKey(req.url, JSON.stringify(records))
    }
  } catch (e) {
    console.log("Unexpected err", e)
  }
  next();
}

export function initRouter(server: restify.Server) {
  server.get('/elections/office', getElectionOffices);
}
