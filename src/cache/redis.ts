import { createClient } from 'redis';

let redisClient;

export async function initRedisConnection() {
  redisClient = createClient({
  })
  redisClient.on('error', (err) => console.log('Redis Client Error', err));
  await redisClient.connect();
}

export async function setKey(key: string, value: string) {
  return redisClient.set(key, value)
}

export async function getKey(key: string) {
  return redisClient.get(key)
}
